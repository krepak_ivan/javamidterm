package kz.aitu.midterm.javamidterm.controller;

import kz.aitu.midterm.javamidterm.model.AuthorizationTable;
import kz.aitu.midterm.javamidterm.repository.AuthorizationTableRepository;
import kz.aitu.midterm.javamidterm.service.AuthorizationTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthorizationTableController {

    @Autowired
    private AuthorizationTableRepository authorizationTableRepository;

    @GetMapping("/tables")
    public String showTables(Model model){
        Iterable<AuthorizationTable> authorizationTables = authorizationTableRepository.findAll();
        model.addAttribute("authorizationTables", authorizationTables);
        return "auth_table";
    }

    //test comment



    private final AuthorizationTableService authorizationTableService;

    public AuthorizationTableController(AuthorizationTableService authorizationTableService) {
        this.authorizationTableService = authorizationTableService;
    }

    @GetMapping("api/authorization_table/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(authorizationTableService.getById(id));
    }

    @GetMapping("api/authorization_table")
    public ResponseEntity<?> findAll(){
        return ResponseEntity.ok(authorizationTableService.getAll());
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @DeleteMapping("api/authorization_table/{id}")
    public void deleteById(@PathVariable long id){
        authorizationTableService.deleteById(id);
    }

    @PostMapping("api/authorization_table")
    public ResponseEntity<?> createAuth(@RequestBody AuthorizationTable authorizationTable){
        return ResponseEntity.ok(authorizationTableService.createAuthorizationTable(authorizationTable));
    }
}
