package kz.aitu.midterm.javamidterm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavamidtermApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavamidtermApplication.class, args);
	}

}
