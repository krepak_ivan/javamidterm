package kz.aitu.midterm.javamidterm.repository;

import kz.aitu.midterm.javamidterm.model.AuthorizationTable;
import org.springframework.data.repository.CrudRepository;

public interface AuthorizationTableRepository extends CrudRepository<AuthorizationTable, Long> {
    AuthorizationTable findById(long id);
}