package kz.aitu.midterm.javamidterm.service;

import kz.aitu.midterm.javamidterm.model.AuthorizationTable;
import kz.aitu.midterm.javamidterm.repository.AuthorizationTableRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationTableService {
    private final AuthorizationTableRepository authorizationTableRepository;

    public AuthorizationTableService(AuthorizationTableRepository authorizationTableRepository) {
        this.authorizationTableRepository = authorizationTableRepository;
    }

    public List<AuthorizationTable> getAll(){
        return (List<AuthorizationTable>) authorizationTableRepository.findAll();
    }

    public AuthorizationTable getById(long id){
        return authorizationTableRepository.findById(id);
    }

    public void deleteById(long id){
        authorizationTableRepository.deleteById(id);
    }

    public AuthorizationTable createAuthorizationTable(AuthorizationTable authorizationTable){
        return authorizationTableRepository.save(authorizationTable);
    }
}
